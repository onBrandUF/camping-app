//All of the campers' information is stored here
var campers = [
{
    "name": "Michael Imperial"
    , "item": "axe"
    , "group": "A",
  }, {
    "name": "Mark Wilkins"
    , "item": "coffee"
    , "group": "A",
  }, {
    "name": "Emma Koza"
    , "item": "bug spray"
    , "group": "A",
  }, {
    "name": "Zoe Simpson"
    , "item": "blankets"
    , "group": "A",
  }, {
    "name": "Alex Janssen"
    , "item": "s/'mores"
    , "group": "B",
  }, {
    "name": "Colin Coller"
    , "item": "bluetooth speaker"
    , "group": "B",
  }, {
    "name": "Nick Evans"
    , "item": "first aid kit"
    , "group": "B",
  }, {
    "name": "Marta Montero"
    , "item": "screetch"
    , "group": "C",
  }, {
    "name": "Jason Hoffman"
    , "item": "Bungee Cords"
    , "group": "C",
  }, {
    "name": "Dylan Foster"
    , "item": "Candy"
    , "group": "C",
  }, {
    "name": "Jess Roycroft"
    , "item": "Adventure Cat"
    , "group": "C",
  }, 

  ];


//document ready 
$(document).ready(function(){
  console.log(campers);
});