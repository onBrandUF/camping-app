all: dev
.PHONY: all

dev: dependencies
	@echo Building for development...
	@npm run dev
.PHONY: dev

prod: dependencies
	@echo Building for production...
	@npm run prod
.PHONY: prod

dependencies:
	@echo Installing npm packages...
	npm install
.PHONY: dependencies

deploy_prod: prod
	@echo Deploying to production...
	@test -s .deploypath || { echo ".deploypath doesn't exist!"; exit 1; }
	@echo `date +%FT%TZ`,`whoami`@`hostname`,`git config user.email`,`git rev-parse --abbrev-ref HEAD` > .last-deployment
	@rsync -rzv --delete --delete-excluded --exclude-from=.deployignore . onbrandserver.uberflip.com:/shared/`cat .deploypath`
	@java -classpath ~/.okta/*.jar com.okta.tools.awscli sts get-caller-identity
	@echo access_key = `grep 'aws_access_key_id' ~/.aws/credentials | awk '{print $$3}'` > ~/.s3cfg
	@echo secret_key = `grep 'aws_secret_access_key' ~/.aws/credentials | awk '{print $$3}'` >> ~/.s3cfg
	@grep 'aws_session_token' ~/.aws/credentials | awk '{print $$3}' > ~/.s3accesstoken
	@s3cmd sync --access_token=`cat ~/.s3accesstoken` --delete-removed --acl-public --exclude-from=.deployignore --cf-invalidate . s3://cihost.uberflip.com/`cat .deploypath`/
.PHONY: deploy_prod

clean:
	@echo Cleaning up...
	git clean -xdf
.PHONY: clean
